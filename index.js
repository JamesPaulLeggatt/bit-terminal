#!/usr/bin/env node
require('dotenv').config() // places all variables on process.env
const setUpCommands = require('./src/setup')

setUpCommands()
