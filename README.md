# Bit Terminal

Welcome to Bit Terminal! A small command line tool for interacting with Bitbucket. Right now, it's _small_ and _simple_. One day it'll be the **rooting tooting** command line tool you need.

## Setup

Setup is easy and quick, simply clone this project. Then navigate into the project and run:

```
$ npm i && npm link
```

After that's all finished your free to go, all that's left now is to run it! 

(That's by running)

```
$ bt
```