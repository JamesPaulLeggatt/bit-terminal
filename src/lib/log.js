const colors = require('colors/safe')

/**
 * Logs green text to the console.
 *
 * @param {String} text - text to log
 */
function success (text) {
  console.log(colors.green(text))
}

/**
 * Logs bold blue text to the console.
 *
 * @param {String} text - text to log
 */
function info (text) {
  console.log(colors.blue.bold(text))
}

/**
 * Logs red text to the console.
 *
 * @param {String} text - text to log
 */
function error (text) {
  console.log(colors.red(text))
}

/**
 * Logs rainbow text to the console.
 *
 * @param {String} text - text to log
 */
function rainbow (text) {
  console.log(colors.rainbow(text))
}

module.exports = {
  success,
  info,
  error,
  rainbow
}
