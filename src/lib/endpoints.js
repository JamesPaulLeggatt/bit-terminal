module.exports = {
  REPOSITORIES: `repositories/${process.env.TEAM_USERNAME}`,
  PRS: `repositories/${process.env.TEAM_USERNAME}/${process.env.REPO_SLUG}/pullrequests?state=OPEN`
}
