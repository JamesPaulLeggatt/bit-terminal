const axios = require('axios')

axios.defaults.baseURL = 'https://api.bitbucket.org/2.0/'

/**
 * Sends a request to the URL passed.
 * Uses ENV parameters for basic auth.
 *
 * @param {string} url - url doesn't include the baseURL.
 *
 * @return {Promise}
 */
function send (url) {
  return axios
    .get(url, {
      auth: {
        username: process.env.USERNAME,
        password: process.env.PASSWORD
      }
    })
}

/**
 * Outputs the Bitbuckets error message if available.
 *
 * @param {Object} error - Error object returned by the Bitbucket API.
 */
function catchError (error) {
  throw error
}

module.exports = {
  send,
  catchError
}
