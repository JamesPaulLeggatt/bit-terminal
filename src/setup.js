const yargs = require('yargs')
const repositories = require('./commands/repositories')
const pullRequests = require('./commands/pull-requests')

function defaultCommand () {
  console.log('No command read, please re-run with a command or try --help')
}

function setUpCommands () {
  return yargs
    .usage('$0 <cmd>')
    .command('$0', '', {}, defaultCommand)
    .command(['allRepos', 'repos'], 'Shows all repos under your TEAM_USERNAME', {}, repositories.getAll)
    .command(['pr'], 'Shows all open Pull requests for the desired REPO_SLUG', pullRequests.get)
    .help()
    .argv
}

module.exports = setUpCommands
