const table = require('table')
const request = require('../lib/request')
const endpoints = require('../lib/endpoints')
const log = require('../lib/log')

/**
 * Gets all open pull requests via the REPO_SLUG env variable.
 */
function get () {
  log.info('Getting all active Pull requests')
  request.send(endpoints.PRS)
    .then(processPullRequests)
    .catch(request.catchError)
}

/**
 * Processes all the active pull requests.
 *
 * @param {Object} response
 */
function processPullRequests (response) {
  const { data: { values } } = response

  if (!values.length) {
    log.success(`No active pull requests for ${process.env.REPO_SLUG}`)
    return false
  }

  log.success('Pull requests found!')

  const tableHeadings = ['Title', 'Target Branch', 'Comment Count', 'Created', 'Approvals', 'URL']
  const pullRequests = values.map(buildPullRequests)

  Promise.all(pullRequests).then((updated) => {
    const tableData = [[...tableHeadings], ...updated]

    console.log(table.table(tableData))
  })
}

/**
 * Builds the pull request table structure.
 *
 * @param {Array} pullRequest
 */
async function buildPullRequests (pullRequest) {
  const participants = await getParticipants(pullRequest.links.self.href)

  return [
    pullRequest.title,
    pullRequest.destination.branch.name,
    pullRequest.comment_count,
    new Date(pullRequest.created_on).toString(),
    getApprovalCount(participants),
    pullRequest.links.html.href
  ]
}

/**
 * Gets the participants of a Pull request
 *
 * @param {Array} approvalUrl
 */
async function getParticipants (approvalUrl) {
  const response = await request.send(approvalUrl)

  return response.data.participants
}

/**
 * Gets the count for the amount of approved participants
 *
 * @param {Number} participants
 */
function getApprovalCount (participants) {
  const approvedParticipants = participants.filter(function (participant) {
    return participant.approved
  })

  return approvedParticipants.length
}

module.exports = {
  get
}
