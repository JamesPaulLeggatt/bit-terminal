const table = require('table')
const pretty = require('prettysize')
const request = require('../lib/request')
const endpoints = require('../lib/endpoints')
const log = require('../lib/log')

/**
 * Requests all the repositories under the scope of
 * the TEAM_USERNAME inside the env file.
 */
function getAll () {
  log.info('Getting all repositories...')
  request.send(endpoints.REPOSITORIES)
    .then(processRepositories)
    .catch(request.catchError)
}

/**
 * Processes the response object from the Bitbucket repositories request
 * and outputs the values to the user.
 *
 * @param {Object} response
 */
function processRepositories (response) {
  log.success('Repositories recieved!')

  const REPO_TABLE_HEADINGS = ['Name', 'Main Branch', 'Url', 'Repo Slug', 'Size', 'Updated on']
  const repoData = response.data.values.map(function (repo) {
    return [
      repo.name,
      repo.mainbranch.name,
      repo.links.html.href,
      repo.slug,
      pretty(repo.size),
      new Date(repo.updated_on).toString()
    ]
  })

  const tableData = [[...REPO_TABLE_HEADINGS], ...repoData]

  console.log(table.table(tableData))
}

module.exports = {
  getAll
}
