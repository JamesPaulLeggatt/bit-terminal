import { assert } from 'chai'
import sinon from 'sinon'
import nock from 'nock'
import * as Table from 'table'
import repositories from '../src/commands/repositories'
import log from '../src/lib/log'
import request from '../src/lib/request'
import responseStub from './stub/repositories'

describe('Repositories spec', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    nock.cleanAll()
    sandbox.restore()
  })

  it('should call log.info and request.send', () => {
    sandbox.stub(Table, 'table')
    const logSpy = sandbox.stub(log)
    const requestStub = sandbox.stub(request, 'send')
      .callsFake(function () {
        return Promise.resolve({ data: responseStub })
      })

    repositories.getAll()

    assert.isTrue(logSpy.info.calledOnceWith('Getting all repositories...'))
    assert.isTrue(requestStub.calledOnce)
  })

  it('should call log.success and pass tableData to table', (done) => {
    const logStub = sandbox.stub(log, 'success')
    const tableStub = sandbox.stub(Table, 'table')
    nock('https://api.bitbucket.org/2.0/')
      .get('/repositories/undefined')
      .reply(200, responseStub)

    repositories.getAll()

    setTimeout(() => {
      assert.isTrue(logStub.calledOnce)
      assert.isTrue(tableStub.calledOnce)
      done()
    }, 80)
  })
})
