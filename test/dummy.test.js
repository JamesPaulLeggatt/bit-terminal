import { assert } from 'chai'

describe('Ensure mocha is setup', () => {
  it('should expect the array length is 4', () => {
    const numbers = [1, 2, 3, 4]
    assert.strictEqual(numbers.length, 4)
  })
})
