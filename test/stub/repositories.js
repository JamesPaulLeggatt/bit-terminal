module.exports = {
  'pagelen': 10,
  'values': [
    {
      'name': 'NoteApp',
      'links': {
        'html': {
          'href': 'https://bitbucket.org/Test/noteapp'
        }
      },
      'mainbranch': {
        'type': 'branch',
        'name': 'master'
      },
      'updated_on': '2016-12-02T10:33:52.159516+00:00',
      'size': 2265506,
      'slug': 'noteapp'
    },
    {
      'name': 'MovieSearch',
      'links': {
        'html': {
          'href': 'https://bitbucket.org/Test/moviesearch'
        }
      },
      'mainbranch': {
        'type': 'branch',
        'name': 'master'
      },
      'updated_on': '2016-01-11T11:40:34.132342+00:00',
      'size': 452481,
      'slug': 'moviesearch'
    },
    {
      'name': 'terminal-app',
      'links': {
        'html': {
          'href': 'https://bitbucket.org/Test/terminal-reader'
        }
      },
      'mainbranch': {
        'type': 'branch',
        'name': 'master'
      },
      'updated_on': '2018-04-25T22:37:46.306648+00:00',
      'size': 447003,
      'slug': 'terminal-reader'
    }
  ],
  'page': 1,
  'size': 3
}
