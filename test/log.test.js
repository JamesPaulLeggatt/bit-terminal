import { assert } from 'chai'
import sinon from 'sinon'
import colors from 'colors/safe'
import log from '../src/lib/log'

describe('Log test', () => {
  let spy
  beforeEach(() => {
    spy = sinon.spy(console, 'log')
  })

  afterEach(() => {
    spy.restore()
  })

  it('should call console.log with green text', () => {
    log.success('Text')
    assert.isTrue(spy.calledOnceWith(colors.green('Text')))
  })

  it('should call console.log with red text', () => {
    log.error('Text')
    assert.isTrue(spy.calledOnceWith(colors.red('Text')))
  })

  it('should call console.log with bold blue text', () => {
    log.info('Text')
    assert.isTrue(spy.calledOnceWith(colors.blue.bold('Text')))
  })

  it('should call console.log with rainbow text', () => {
    log.rainbow('Text')
    assert.isTrue(spy.calledOnceWith(colors.rainbow('Text')))
  })
})
