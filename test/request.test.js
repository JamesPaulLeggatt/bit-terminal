import { assert } from 'chai'
import sinon from 'sinon'
import axios from 'axios'
import nock from 'nock'
import request from '../src/lib/request'

describe('Request test', () => {
  let axiosSpy
  beforeEach(() => {
    nock.cleanAll()
    axiosSpy = sinon.spy(axios, 'get')
  })

  afterEach(() => {
    axiosSpy.restore()
  })

  it('should call axios', async () => {
    const url = '/repositories/Test'

    nock('https://api.bitbucket.org/2.0/')
      .get(url)
      .reply(200)

    await request.send(url)

    assert.isTrue(axiosSpy.calledOnce)
  })
})
